# PIC 10A: Introduction to programming (fall 2018)

This is the _unofficial_ class website. Here you will find handouts, slides, and
code related to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus]
*   [Walk troughs, links & handouts.][handouts] Includes practice material for
    midterms.
*   [Lessons: pdf files & Google slide presentations][lessons]
*   [Class assignments][hw]

[syllabus]: ./syllabus/fall18-pic10a.pdf
[handouts]: https://ccle.ucla.edu/course/view.php?id=65105&section=2
[lessons]: ./lessons
[hw]: https://ccle.ucla.edu/course/view.php?id=65105&section=4


## Additional resources

Here are other sites associated to our course:

*   [The _official_ CCLE class website][CCLE].
*   [Discussion section material (examples, code, etc.)][ds]

[CCLE]: https://ccle.ucla.edu/course/view/18F-COMPTNG10A-4
[ds]: https://ccle.ucla.edu/course/view.php?id=65105&section=6
