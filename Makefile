# Makefile to manage the pic10c website

SYLL_D=         syllabus
SYLL_PDF=       $(SYLL_D)/fall18-pic10b.pdf
SYLL_MD=        $(SYLL_D)/readme.md
SYLL_RAW=       $(SYLL_D)/syllabus.txt
SYLL_HTML=      $(SYLL_D)/index.html
CSS_FILE=       pandoc.css

.PHONY: all debug update_syllabus

all: index.html

index.html: readme.md $(CSS_FILE)
	@echo Converting $< to $@
	@pandoc -s --css $(CSS_FILE) --quiet -o $@ $<


debug:
	@echo Directory: $(SYLL_D)
	@echo PDF: $(SYLL_PDF)
	@echo MD: $(SYLL_MD)
	@echo RAW: $(SYLL_RAW)
	@echo CSS: $(CSS_FILE)


update_syllabus: $(SYLL_PDF) $(SYLL_RAW) $(SYLL_HTML)


$(SYLL_PDF): $(SYLL_MD)
	@cd $(SYLL_D) && ./create_pdf.sh


$(SYLL_RAW): $(SYLL_MD)
	@cp $< $@ 


$(SYLL_HTML): $(SYLL_MD)
	@echo Converting $< to $@
	@pandoc -s --css $(CSS_FILE) --quiet -o $@ $<
