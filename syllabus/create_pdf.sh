#!/bin/bash
#
# Pandoc command to convert readme.md to a .pdf file
pandoc -f markdown+fenced_code_attributes+fancy_lists+startnum+pipe_tables+yaml_metadata_block \
  --highlight-style tango \
  --variable monofont=Inconsolata \
  --variable papersize=letter \
  --variable fontsize=12pt \
  --variable colorlinks \
  --variable geometry=centering \
  --variable geometry="scale=0.75" \
  "readme.md" --latex-engine=xelatex -o "fall18-pic10b.pdf"
