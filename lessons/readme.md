# Course lessons (slides)

This is place to look for class slides that I have been using during lecture.
Lessons 1 through 3 were prepared using LibreOffice, an open source _Office
Suite_. The actual presentations are saved in `.odp` files, and even though they
can be converted to other formats (_e.g.,_ Microsoft Office&reg; Power
Point&reg;), sometimes the resulting set of slides differs a lot from the
original set. 

To try to minimize the difference between whet you see during lecture, and what
you see here, I am providing `.pdf` files taken from the original set of slides.
However, be aware that this files only contain one page per slide: for example,
if during lecture there was a pause in one of the slides, and after the pause
new content appeared _on top_ of other content, the covered content will not
appear in the `.pdf` files.

Here are links corresponding to the first three lessons:

1.  [Lesson 1][l1]: _Hello world!_, line by line.
2.  [Lesson 2][l2]: Variables, and Input/Output.
3.  [Lesson 3][l3]: Arithmetic & casting.

[l1]: lesson01.pdf
[l2]: lesson02.pdf
[l3]: lesson03.pdf

The rest of the lessons were prepared using Google Slides. As we discuss their
contents during lecture, I will provide links to the actual slides. Once a set
of slides has been thoroughly discussed, a `.pdf` file will be made available to
the class.

4.  [Lesson 4][l4]: String classes, and the `string` class ([Google slides][l4],
    [pdf file][l4-pdf]).

5.  [Lesson 5:][l5] Classes: `std::string`, and the **c**omputing
    **c**oncepts in **c**++ (or `ccc` for short) classes[^one].
    
    -   Use these [Google slides][l5-tempo], to prepare for the midterm. They do
        not include material related to the `ccc` classes.
    -   Alternatively, you can download the [pdf version][l5-tempo-pdf].

6.  [Lesson 6:][l6] Graphing I. Introduction to graphing ([Google slides][l6],
    [pdf file][l6-pdf]).
7.  [Lesson 7:][l7] Graphing II. More complex shapes ([Google slides][l7],
    [pdf file][l7-pdf]).
8.  [Lesson 8][l8]: Decisions I: The `if` statement ([Google slides][l8],
    [pdf file][l8-pdf]).
9.  [Lesson 9:][l9] Comparing data types, De Morgan's laws, the `while` loop
    ([Google slides][l9], [pdf file][l9-pdf]).
10. [Lesson 10:][l10] More loops: `do-while`, `for`. Decisions II: the `switch`
    statement ([Google slides][l10], [pdf file][l10-pdf]).
11. [Lesson 11:][l11] Functions ([Google slides][l11], [pdf file][l11-pdf]).
12. [Lesson 12:][l12] Value and reference parameters ([Google slides][l12],
    [pdf file][l12-pdf]).
13. [Lesson 13:][l13] Random numbers ([Google slides][l13],
    [pdf file][l13-pdf]).
14. [Lesson 14:][l14] Classes: user defined classes ([Google slides][l14],
    [pdf file][l14-pdf]).
15. [Lesson 15:][l15] Classes: function overloading, implicit and explicit
    parameters, member and non-member functions, separate compilation ([Google
    slides][l15], [pdf file][l15-pdf]).
16. [Lesson 16:][l16] Vectors ([Google slides][l16], [pdf file][l16-pdf]).
17. [Lesson 17:][l17] Arrays ([Google slides][l17], [pdf file][l17-pdf]).
18. [Lesson 18:][l18] Files I/O ([Google slides][l18], [pdf file][l18-pdf]).

[l4]: https://docs.google.com/presentation/d/e/2PACX-1vSD1F8Rs8al-CVvfZ_WMQnHfjTMrYn7497jjUJsC1A6QRX-tWgpFPCd_N7rwMkkb9L0KjxNyHtiI_jG/pub?start=false&loop=false&delayms=3000
[l5]: https://docs.google.com/presentation/d/e/2PACX-1vQ-hLV4UbW_LN7xle188fp2TMVxhx-teMFpCs-CLMJRX8uW9QODZQyDWjyVEgPsdB17nJgnc4htESIA/pub?start=false&loop=false&delayms=3000
[l5-tempo]: https://docs.google.com/presentation/d/e/2PACX-1vRqeQCRu7en4lrB4x1VkySjZqkuhwOaDcqMMVrULlQKFQAvKvxsYHgMADbL2yIHDWgkTxBLL3KDKcDN/pub?start=false&loop=false&delayms=30000
[l6]: https://docs.google.com/presentation/d/e/2PACX-1vR84ZlW3wNV6YMwW1fSDe4Q-XYdBuDSJ7BBjpg6vWlc9tSVhjEpnUTlMXk3bcfr_vaEl6xVdPbwkWD6/pub?start=false&loop=false&delayms=3000
[l7]: https://docs.google.com/presentation/d/e/2PACX-1vQyfqNQdkft0pWiQT_xO2wrwcfifTi9HmaRQVdUBpa8URgVpLhX7DXN3C0FsfqiuFqo8xYF_5DJ8hmw/pub?start=false&loop=false&delayms=3000
[l8]: https://docs.google.com/presentation/d/e/2PACX-1vQDhEs2CKNim0O4WWEnBix6FOxnIn6UKH5CdHeBreToLqNjpFHI0TFe--2gktUALoJueC5Ev_OOKzYE/pub?start=false&loop=false&delayms=3000
[l9]: https://docs.google.com/presentation/d/e/2PACX-1vTWetziVMJy9FrJ29RgkzNEHwYh9TZYZn1bkDBtryUHQ_mOY3idtlgj9SiWxKvIVrcIN88EWMiLgTv6/pub?start=false&loop=false&delayms=3000
[l10]: https://docs.google.com/presentation/d/e/2PACX-1vTfW7tyRXKuT_IelaHuwrN-PrFwZlib-cSR_LZn-xl1XCGaplETvG6AXIDnV1SWC1Q1pXuEmBCdIkYy/pub?start=false&loop=false&delayms=3000
[l11]: https://docs.google.com/presentation/d/e/2PACX-1vRU8BgN2aLCfeX2trVgk_Rjqctj6IY7dQJaCpkpbvq6k6xVAoRfy_gBJaAJ9ciFdfGQphEyQgrqPnxz/pub?start=false&loop=false&delayms=3000
[l12]: https://docs.google.com/presentation/d/e/2PACX-1vT4CQejRb6Ty6aDuwjTsJkK5Jt91JxaA92Ub0XayTgWKY8CyB9UEM-B-xijXxu2trRuKt9CH_hxAeB_/pub?start=false&loop=false&delayms=3000
[l13]: https://docs.google.com/presentation/d/e/2PACX-1vQS8gtwNahU__x9Jbux8Ql3BpG32CTB-WbxWs4aymaH6x2Le8zfkMm7xL5w12QOalsgokzxaWEDU8bR/pub?start=false&loop=false&delayms=3000
[l14]: https://docs.google.com/presentation/d/e/2PACX-1vRCBbvY88coPNQ0LHuyA-7AQl-ZyMLIHEngar7srd8RPsp6ZlWd9cAmOxdJOUk21BtKw8r9oOyFUmN2/pub?start=false&loop=false&delayms=3000
[l15]: https://docs.google.com/presentation/d/e/2PACX-1vSUOwvLxGNO_2olF4TAvJaG-IbrVZGsRNFX5ApD1xsYDHFmmTNsjLDQwOg6e9SeWGT38eu9JggdKGPR/pub?start=false&loop=false&delayms=3000
[l16]: https://docs.google.com/presentation/d/e/2PACX-1vSUOwvLxGNO_2olF4TAvJaG-IbrVZGsRNFX5ApD1xsYDHFmmTNsjLDQwOg6e9SeWGT38eu9JggdKGPR/pub?start=false&loop=false&delayms=3000
[l17]: https://docs.google.com/presentation/d/e/2PACX-1vSLShBJscGzVU3AWsq7XJ3smnwUSJmPlqr_0EWgHX9kKsrL7FBDPUX6mKc1-64eFGEHeKp1ucCdr6M1/pub?start=false&loop=false&delayms=3000
[l18]: https://docs.google.com/presentation/d/e/2PACX-1vSTGWhSS0hQN2-cr2bL5nPptzRtFP9e4bV6X1F-WfHWjGLalz0eqxZnYBLpkyBy9TeUk4ODOaVV8ELV/pub?start=false&loop=false&delayms=3000

[l4-pdf]: lesson04.pdf
[l5-tempo-pdf]: lesson05-tempo.pdf
[l6-pdf]: lesson06.pdf
[l7-pdf]: lesson07.pdf
[l8-pdf]: lesson08.pdf
[l9-pdf]: lesson09.pdf
[l10-pdf]: lesson10.pdf
[l11-pdf]: lesson11.pdf
[l12-pdf]: lesson12.pdf
[l13-pdf]: lesson13.pdf
[l14-pdf]: lesson14.pdf
[l15-pdf]: lesson15.pdf
[l16-pdf]: lesson16.pdf
[l17-pdf]: lesson17.pdf
[l18-pdf]: lesson18.pdf

[^one]: Please be aware that these classes are provided by Horstmann (the author
  of the textbook). In particular, they are not considered to be _"part of"_
  C++.
